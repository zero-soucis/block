//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
//	{"", "pacupdate.sh",		1800,		            9},
	{"⌨️ "  , "setxkbmap -query | grep layout | awk '{print $2}'",	    6,     1},
	{" ", "memory.sh",	    6,		              1},
	{" ", "battery.sh",	    6,		              1},
	{"", "volume.sh",			2,		              10},
	{" ", "clock.sh",		  5,		              0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
